import React, { useState, Component, } from "react";

import axios from "axios"


import {
  TextInput,
  ActivityIndicator,
  Button,

  Image,
  Share,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'


export default class App extends Component {

  constructor(props) {

    super(props)

    this.state = {
      category: '',
      name: '',
      selectedfile: '',

      rent: '',
      price: '',
      loc: '',
      qty: '',
      image: null,
      uploading: false,
      text: ''
    }

  }


  name = event1 => {
    this.state.name = event1.target.value;
  }
  category = event1 => {
    this.state.category = event1.target.value;
  }
  price = event1 => {
    this.state.price = event1.target.value;
  }
  rent = event1 => {
    this.state.rent = event1.target.value;
  }
  loc = event1 => {
    this.state.loc = event1.target.value;
  }
  QTY = event1 => {
    this.state.qty = event1.target.value;
  }

  fileuploadhandler = () => {
    // const { selectedfile } = this.state;
    const { category } = this.state;
    const { name } = this.state;
    const { price } = this.state;
    const { loc } = this.state;
    const { rent } = this.state;
    const { qty } = this.state

    alert(this.state.selectedfile)
    const fb = new FormData();
    let uriParts = this.state.selectedfile.split('.');
    let fileType = uriParts[uriParts.length - 1];
    fb.append('photo', this.state.selectedfile);
    alert(this.state.selectedfile)
    // fb.append('profile', this.state.selectedfile);
    fb.append('category', category);
    fb.append('name', name);
    fb.append('price', price);

    fb.append('rent', rent);
    fb.append('loc', loc);
    fb.append('qty', 'qty');


    //alert(fb)
    axios.post('http://localhost:4000/upload', fb, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      }
    }).then(res => {
      alert(res.data);
      console.log(fb)
    });


  }



  render() {
    let {
      image
    } = this.state;

    return (
      <View style={{ backgroundColor: '#b19cd9 ', flex: 1, justifyContent: 'center', position: "absolute", top: 40, height: 450, width: 350, borderRadius: 10, margin: 5 }}>

        <Text style={styles.nametext6}>CATEG:</Text>
        <Text style={styles.nametext}>NAME :</Text>
        <TextInput style={styles.inputStyle} onChange={this.name} />
        <Text style={styles.nametext1}>RENT :</Text>
        <TextInput style={styles.inputStyle1} onChange={this.rent} />
        <Text style={styles.nametext2}>LOCAL :</Text>
        <TextInput style={styles.inputStyle2} onChange={this.loc} />
        <Text style={styles.nametext3}>PRICE :</Text>
        <TextInput style={styles.inputStyle3} onChange={this.price} />
        <Text style={styles.nametext4}>QTY :</Text>
        <TextInput style={styles.inputStyle4} onChange={this.QTY} />
        <TextInput style={styles.inputStyle5} onChange={this.category} />




        <TouchableOpacity style={styles.TouchView1} onPress={this._pickImage}>
          <Text style={styles.TextView1}>Pickfile</Text>
        </TouchableOpacity >

        <TouchableOpacity
          onPress={this.fileuploadhandler}
          style={styles.TouchView2}>
          <Text style={styles.TextView2} >upload</Text>
        </TouchableOpacity>
      </View>

    );

  }

  _maybeRenderUploadingOverlay = () => {
    if (this.state.uploading) {
      return (
        <View
          style={[StyleSheet.absoluteFill, styles.maybeRenderUploading]}>
          <ActivityIndicator color="#fff" size="large" />
        </View>
      );
    }
  };

  _maybeRenderImage = () => {
    let {
      image
    } = this.state;

    if (!image) {
      return;
    }

    return (
      <View
        style={styles.maybeRenderContainer}>
        <View
          style={styles.maybeRenderImageContainer}>
          <Image source={{ uri: image }} style={styles.maybeRenderImage} />
        </View>

        <Text
          onPress={this._copyToClipboard}
          onLongPress={this._share}
          style={styles.maybeRenderImageText}>
          {image}
        </Text>
      </View>
    );
  };

  _share = () => {
    Share.share({
      message: this.state.image,
      title: 'Check out this photo',
      url: this.state.image,
    });
  };

  _copyToClipboard = () => {
    Clipboard.setString(this.state.image);
    alert('Copied image URL to clipboard');
  };

  _takePhoto = async () => {
    const {
      status: cameraPerm
    } = await Permissions.askAsync(Permissions.CAMERA);

    const {
      status: cameraRollPerm
    } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    // only if user allows permission to camera AND camera roll
    if (cameraPerm === 'granted' && cameraRollPerm === 'granted') {
      let pickerResult = await ImagePicker.launchCameraAsync({
        allowsEditing: true,
        aspect: [4, 3],
      });

      this._handleImagePicked(pickerResult);
    }
  };

  _pickImage = async () => {

    // only if user allows permission to camera roll
    // if (cameraRollPerm === 'granted')
    {
      let pickerResult = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3],
      });

      this._handleImagePicked(pickerResult);
    }
  };
  async uploadImageAsync(uri) {
    //  alert(uri)
    this.state.selectedfile = uri;
    // alert(this.state.selectedfile)

  }
  _handleImagePicked = async pickerResult => {
    let uploadResponse, uploadResult;

    try {
      this.setState({
        uploading: true
      });

      if (!pickerResult.cancelled) {
        alert(pickerResult.uri)
        console.log(pickerResult.uri)
        uploadResponse = await this.uploadImageAsync(pickerResult.uri);
        //   uploadResult = await uploadResponse.json();
        // this.setState({
        //   image: uploadResult.location
        // });
      }
    } catch (e) {
      console.log({ uploadResponse });
      console.log({ uploadResult });
      console.log({ e });
      alert('Upload failed, sorry :(' + e);
    } finally {
      this.setState({
        uploading: false
      });
    }
  };
}

// async function uploadImageAsync(uri) {
//   alert(uri)
//   let apiUrl = 'http://ead269951299.ngrok.io/upload';

//   // Note:
//   // Uncomment this if you want to experiment with local server
//   //
//   // if (Constants.isDevice) {
//   //   apiUrl = `https://your-ngrok-subdomain.ngrok.io/upload`;
//   // } else {
//   //   apiUrl = `http://localhost:3000/upload`
//   // }

//   let uriParts = uri.split('.');
//   let fileType = uriParts[uriParts.length - 1];

//   let formData = new FormData();
//   formData.append('photo', {
//     uri,
//     name: `photo.${fileType}`,
//     type: `image/${fileType}`,
//   });

//   let options = {
//     method: 'POST',
//     body: formData,
//     headers: {
//       Accept: 'application/json',
//       'Content-Type': 'multipart/form-data',
//     },
//   };

//   return fetch(apiUrl, options);
// }

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  exampleText: {
    fontSize: 20,
    marginBottom: 20,
    marginHorizontal: 15,
    textAlign: 'center',
  },
  maybeRenderUploading: {
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'center',
  },
  maybeRenderContainer: {
    borderRadius: 3,
    elevation: 2,
    marginTop: 30,
    shadowColor: 'rgba(0,0,0,1)',
    shadowOpacity: 0.2,
    shadowOffset: {
      height: 4,
      width: 4,
    },
    shadowRadius: 5,
    width: 250,
  },
  maybeRenderImageContainer: {
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    overflow: 'hidden',
  },
  maybeRenderImage: {
    height: 250,
    width: 250,
  },
  maybeRenderImageText: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  inputStyle: {
    backgroundColor: 'white',
    height: 35,
    width: 240,
    borderWidth: 1,
    borderColor: "white",
    borderRadius: 20,
    paddingLeft: 20,
    margin: 100,
    bottom: 250,
    position: "absolute",
    borderColor: '#05075d',
  },
  inputStyle5: {
    backgroundColor: 'white',
    height: 35,
    width: 240,
    borderWidth: 1,
    borderColor: "white",
    borderRadius: 20,
    margin: 100,
    bottom: 250,
    position: "absolute",
    borderColor: '#05075d',
    paddingLeft: 20,
  },
  nametext: {
    bottom: 340,
    color: "purple",
    fontWeight: "bold",
    fontSize: 20,
    margin: 20,
    position: "absolute"
  },
  nametext1: {
    bottom: 285,
    fontSize: 20,
    margin: 20,
    color: "purple",
    fontWeight: "bold",
    position: "absolute"
  },
  nametext2: {
    bottom: 235,
    fontSize: 20,
    margin: 20,
    color: "purple",
    fontWeight: "bold",
    position: "absolute"
  },
  nametext3: {
    bottom: 185,
    fontSize: 20,
    margin: 20,
    color: "purple",
    fontWeight: "bold",
    position: "absolute"
  },
  nametext4: {
    bottom: 140,
    fontSize: 20,
    margin: 20,
    color: "purple",
    fontWeight: "bold",
    position: "absolute"
  },
  nametext5: {
    bottom: 100,
    fontSize: 20,
    margin: 20,
    color: "purple",
    fontWeight: "bold",
    position: "absolute"
  },
  nametext6: {
    bottom: 100,
    fontSize: 20,
    margin: 20,
    color: "purple",
    fontWeight: "bold",
    position: "absolute"
  },

  inputStyle1: {
    backgroundColor: 'white',
    height: 35,
    width: 240,
    borderWidth: 1,
    borderColor: "white",
    borderRadius: 20,
    margin: 100,
    bottom: 200,
    position: "absolute",
    borderColor: '#05075d',
    paddingLeft: 20,
  },
  inputStyle2: {
    backgroundColor: 'white',
    height: 35,
    width: 240,
    borderWidth: 1,
    borderColor: "white",
    borderRadius: 20,
    margin: 100,
    bottom: 150,
    position: "absolute",
    borderColor: '#05075d',
    paddingLeft: 20,
  },
  inputStyle3: {
    backgroundColor: 'white',
    height: 35,
    width: 240,
    borderWidth: 1,
    borderColor: "white",
    borderRadius: 20,
    margin: 100,
    bottom: 100,
    position: "absolute",
    borderColor: '#05075d',
    paddingLeft: 20,
  },
  inputStyle4: {
    backgroundColor: 'white',
    height: 30,
    width: 240,
    borderWidth: 1,
    borderColor: "white",
    borderRadius: 20,
    margin: 100,
    bottom: 60,
    position: "absolute",
    borderColor: '#05075d',
    paddingLeft: 20,
  },
  inputStyle5: {
    backgroundColor: 'white',
    height: 30,
    width: 240,
    borderWidth: 1,
    borderColor: "white",
    borderRadius: 20,
    margin: 100,
    bottom: 20,
    position: "absolute",
    borderColor: '#05075d',
    paddingLeft: 20,



  },

  TouchView1: {
    borderRadius: 15,
    height: 30,
    backgroundColor: "#05075d",
    width: 150
  },
  TouchView1: {
    borderRadius: 15,
    height: 30,
    backgroundColor: "#05075d",
    width: 150,
    top: 340,
    margin: 15,
    position: "absolute"
  },
  TextView1: {
    textAlign: "center",
    fontSize: 20,
    position: "absolute",
    left: 40,
    color: "#fff"
  },
  TouchView2: {
    borderRadius: 15,
    height: 30,
    backgroundColor: "#05075d",
    width: 150,
    top: 175,
    margin: 180,
    position: "absolute"

  },
  TextView2: {
    textAlign: "center",
    fontSize: 20,
    position: "absolute",
    left: 40,
    color: "#fff"
  },
  mainText: {
    bottom: 400,
    fontSize: 25,
    textAlign: "center",
    fontStyle: "normal",
    position: "absolute",
    margin: 40,
    padding: 10
  },
  TouchView3: {
    borderRadius: 15,
    height: 30,
    backgroundColor: "#05075d",
    width: 190,
    top: 280,
    margin: 110,
    position: "absolute"
  },
  TextView3: {
    textAlign: "center",
    fontSize: 20,
    position: "absolute",
    left: 40,
    color: "#fff"
  }

});

